# Patchwork archive

This is a static copy of patchwork.archlinux.org created using `wget`, prior to its [decommission](https://lists.archlinux.org/archives/list/arch-dev-public@lists.archlinux.org/message/7B6R5HVEC67U7B2VQ3SKUVXU4RDCRRMM/), on December 29, 2022 between 15:54 and 18:28 UTC.
