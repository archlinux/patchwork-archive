diff --git a/.gitignore b/.gitignore
index 1e92b82..04eefe5 100644
--- a/.gitignore
+++ b/.gitignore
@@ -16,3 +16,4 @@ zsh_completion
 find-libdeps
 crossrepomove
 arch-nspawn
+doc/*.1
diff --git a/Makefile b/Makefile
index 2d29c83..80fe1d7 100644
--- a/Makefile
+++ b/Makefile
@@ -1,6 +1,7 @@
 V=20180531
 
 PREFIX = /usr/local
+MANDIR = $(PREFIX)/share/man
 
 BINPROGS = \
 	checkpkg \
@@ -60,7 +61,13 @@ BASHCOMPLETION_LINKS = \
 	archco \
 	communityco
 
-all: $(BINPROGS) bash_completion zsh_completion
+
+MANS = \
+	doc/lddd.1
+
+
+all: $(BINPROGS) bash_completion zsh_completion man
+man: $(MANS)
 
 edit = sed -e "s|@pkgdatadir[@]|$(DESTDIR)$(PREFIX)/share/devtools|g"
 
@@ -72,8 +79,13 @@ edit = sed -e "s|@pkgdatadir[@]|$(DESTDIR)$(PREFIX)/share/devtools|g"
 	@chmod +x "$@"
 	@bash -O extglob -n "$@"
 
+$(MANS): doc/asciidoc.conf doc/footer.asciidoc
+
+doc/%: doc/%.asciidoc
+	a2x --no-xmllint --asciidoc-opts="-f doc/asciidoc.conf" -d manpage -f manpage -D doc $<
+
 clean:
-	rm -f $(BINPROGS) bash_completion zsh_completion
+	rm -f $(BINPROGS) bash_completion zsh_completion $(MANS)
 
 install:
 	install -dm0755 $(DESTDIR)$(PREFIX)/bin
@@ -88,6 +100,9 @@ install:
 	for l in ${BASHCOMPLETION_LINKS}; do ln -sf devtools $(DESTDIR)/usr/share/bash-completion/completions/$$l; done
 	install -Dm0644 zsh_completion $(DESTDIR)$(PREFIX)/share/zsh/site-functions/_devtools
 	ln -sf archco $(DESTDIR)$(PREFIX)/bin/communityco
+	for manfile in $(MANS); do \
+		install -Dm644 $$manfile -t $(DESTDIR)$(MANDIR)/man$${manfile##*.}; \
+	done;
 
 uninstall:
 	for f in ${BINPROGS}; do rm -f $(DESTDIR)$(PREFIX)/bin/$$f; done
@@ -99,6 +114,9 @@ uninstall:
 	rm $(DESTDIR)$(PREFIX)/share/zsh/site-functions/_devtools
 	rm -f $(DESTDIR)$(PREFIX)/bin/communityco
 	rm -f $(DESTDIR)$(PREFIX)/bin/find-libprovides
+	for manfile in $(MANS); do \
+		rm -f $(DESTDIR)$(MANDIR)/man$${manfile##*.}/$${manfile#doc/}; \
+	done;
 
 dist:
 	git archive --format=tar --prefix=devtools-$(V)/ $(V) | gzip -9 > devtools-$(V).tar.gz
diff --git a/doc/asciidoc.conf b/doc/asciidoc.conf
new file mode 100644
index 0000000..c675a20
--- /dev/null
+++ b/doc/asciidoc.conf
@@ -0,0 +1,37 @@
+## linkman: macro
+# Inspired by/borrowed from the GIT source tree at Documentation/asciidoc.conf
+#
+# Usage: linkman:command[manpage-section]
+#
+# Note, {0} is the manpage section, while {target} is the command.
+#
+# Show man link as: <command>(<section>); if section is defined, else just show
+# the command.
+
+[macros]
+(?su)[\\]?(?P<name>linkman):(?P<target>\S*?)\[(?P<attrlist>.*?)\]=
+
+[attributes]
+asterisk=&#42;
+plus=&#43;
+caret=&#94;
+startsb=&#91;
+endsb=&#93;
+backslash=&#92;
+tilde=&#126;
+apostrophe=&#39;
+backtick=&#96;
+litdd=&#45;&#45;
+
+ifdef::backend-docbook[]
+[linkman-inlinemacro]
+{0%{target}}
+{0#<citerefentry>}
+{0#<refentrytitle>{target}</refentrytitle><manvolnum>{0}</manvolnum>}
+{0#</citerefentry>}
+endif::backend-docbook[]
+
+ifdef::backend-xhtml11[]
+[linkman-inlinemacro]
+<a href="{target}.{0}.html">{target}{0?({0})}</a>
+endif::backend-xhtml11[]
diff --git a/doc/footer.asciidoc b/doc/footer.asciidoc
new file mode 100644
index 0000000..a092447
--- /dev/null
+++ b/doc/footer.asciidoc
@@ -0,0 +1,28 @@
+
+Bugs
+----
+Bugs can be reported on the bug tracker 'https://bugs.archlinux.org' in the Arch
+Linux category and title prefixed with [devtools] or via
+mailto:arch-projects@archlinux.org[].
+
+
+Authors
+-------
+
+Maintainers:
+
+* Aaron Griffin <aaronmgriffin@gmail.com>
+* Allan McRae <allan@archlinux.org>
+* Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
+* Dan McGee <dan@archlinux.org>
+* Dave Reisner <dreisner@archlinux.org>
+* Evangelos Foutras <evangelos@foutrelis.com>
+* Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
+* Levente Polyak <anthraxx@archlinux.org>
+* Pierre Schmitz <pierre@archlinux.de>
+* Sébastien Luttringer <seblu@seblu.net> 
+* Sven-Hendrik Haase <svenstaro@gmail.com>
+* Thomas Bächler <thomas@archlinux.org>
+
+For additional contributors, use `git shortlog -s` on the devtools.git
+repository.
diff --git a/doc/lddd.1.asciidoc b/doc/lddd.1.asciidoc
new file mode 100644
index 0000000..3a015d2
--- /dev/null
+++ b/doc/lddd.1.asciidoc
@@ -0,0 +1,25 @@
+lddd(1)
+=======
+
+Name
+----
+lddd - Find broken library links on your system
+
+Synopsis
+--------
+lddd
+
+Description
+-----------
+
+Scans '$PATH', '/lib', '/usr/lib', '/usr/local/lib' and
+'/etc/ld.so.conf.d/*.conf' directories for ELF files with references to missing
+shared libraries, and suggests which packages might need to be rebuilt. The
+collected data is written to a temporary directory created by mktemp.
+
+See Also
+--------
+
+linkman:ldd[1]
+
+include::footer.asciidoc[]
